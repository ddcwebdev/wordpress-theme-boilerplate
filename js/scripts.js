(function ($, root, undefined) {
    "use strict";

	if (typeof(stLight)==="undefined"){
		var stLight={};
		stLight.subscribe = function(){
			return;
		};
	}
	
	$(function () {
		
		
		// DOM ready, take it away


		var owlCarousels = [];
		$('.owl-carousel').each(function () {
			var bodyWidth = $('body').outerWidth();
			var showControls = ($(this).data('show-controls'))? ($(this).data('show-controls')) : false;
			var shouldAutoPlay  = ($(this).data('auto-play'))? ($(this).data('auto-play')) : false;

			var individualOwl = $(this).owlCarousel({
				autoplay: shouldAutoPlay,
				autoplayTimeout: 8000,
				autoplayHoverPause: true,
				margin:30,
				nav:showControls,
				dots:false,
				loop:true,
				navText:['&lt;','&gt;'],
				items:1,
				responsiveClass:true

			});
			owlCarousels.push(individualOwl);
		});

		// hide all widget panels except for the first
		var tabsConatiner = $('.tabs-container');
		var tabsLinks = $('.tabs-container').find('.tabs').find('a');

		tabsConatiner.find('.tabs-content:gt(0)').hide();

		tabsLinks.click(function(e) {
			// disable default hyperlink behavior
			e.preventDefault();
			// only execute if the clicked tab is inactive
			if (!$(this).hasClass('active'))
			{
				// set all tabs as "inactive"
				tabsLinks.removeClass('active');
				// set the clicked tab as "active"
				$(this).addClass('active');
				// hide all widget panels
				tabsConatiner.find('.tabs-content').hide();
				// show the widget panel based on the the tab's "href"
                var linkHref = e.target.getAttribute('href');
				$(linkHref).show();
                facebookResize(linkHref);
            
			}
		});

		$('.hot-bar-container').find('a[href="#"]').click(function(e){
			e.preventDefault();
			$('.header-login-form').slideToggle();
		});

		$('.mobile-nav-btn').sidr({
			source: 'nav.nav ul',
			name: 'main-site-nav',
			renaming: false;
		});
		$(".sidr-button-close").on('click',function(){
			$.sidr('close', 'main-site-nav');
		});

            
        
        
	});
	//Script to dynamic resize Facebook page plugin
	
	function facebookResize(targetID){
		var newWidth ="";
		var dw= "";
		
		if (targetID.indexOf('facebook') ==  -1 ) return;        
        var target  = document.getElementById(targetID.replace('#',''));
        var targetWidth = target.offsetWidth;
        
		newWidth = (targetWidth > 500 )?  500 : targetWidth ;
        
		dw = $('.fb-page').attr('data-width');
	
		if (newWidth != dw){
			$('.fb-page').attr('data-width',newWidth);		
			$('.fb-page').attr('fb-iframe-plugin-query','').empty();
			FB.XFBML.parse();
		}
	}
	
	
	
	function debounce(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	}
	
})(jQuery, this);
