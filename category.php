<?php get_header(); ?>

	<main role="main">
		<a id="primary-content" href="javascript:;"></a>

		<!-- section -->
		<section>

			<h1><?php _e( 'Categories for ','ddcabarebones' ); single_cat_title(); ?></h1>
            <?php 
                the_archive_description( '<div class="taxonomy-description">', '</div>' );            
            ?>
			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
