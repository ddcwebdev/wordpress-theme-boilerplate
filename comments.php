<?php
$comment_args = array(
	'id_form' => 'commentform',
	'id_submit' => 'comment-submit-button',
	'title_reply' => __('Leave a Comment'),
	'title_reply_to' => __('Leave a Reply to %s'),
	'cancel_reply_link' => __('Cancel Reply'),
	'label_submit' => __('Submit Comment'),
	'comment_notes_after' => '',
	'comment_notes_before' => '',
	'logged_in_as' => '',

	'comment_field' => '<p class="comment-form-comment">' .
		'<textarea name="comment" id="comment"></textarea>' .
		'</p>',

	'must_log_in' => '<p class="must-log-in">' .
		sprintf(
			__('You must be <a href="%s">logged in</a> to post a comment.'),
			site_url('/login/')
		) . '</p>',


	'fields' => ''
);
?>

<div class="comments">
	<?php if (post_password_required()) : ?>
	<p><?php _e( 'Post is password protected. Enter the password to view any comments.', 'ddcabarebones' ); ?></p>
</div>

	<?php return; endif; ?>

<?php if (have_comments()) : ?>

	<h2><?php comments_number(); ?></h2>

	<ul>
		<?php wp_list_comments( array( 'callback' => 'ddcabarebonescomments', 'style' => 'ul', 'per_page' => 10, //Allow comment pagination
            'reverse_top_level' => false, 'avatar_size' => 40 ) , $comments); // Custom callback in functions.php ?>
	</ul>

<?php elseif ( ! comments_open() && ! is_page() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

	<p><?php _e( 'Comments are closed here.','ddcabarebones' ); ?></p>

<?php endif; ?>

<?php comment_form($comment_args); ?>

</div>
