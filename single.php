<?php get_header(); ?>

	<main role="main">
        <a id="primary-content" href="javascript:;"></a>

	<!-- section -->
        <section>
    
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
    
            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    
                <!-- post thumbnail -->
                <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <?php the_post_thumbnail(); // Fullsize image for the single post ?>
                    </a>
                <?php endif; ?>
                <!-- /post thumbnail -->
    
                <!-- post title -->
                <h1>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(array('post' => $post->ID,'echo' => false)); ?>"><?php the_title(); ?></a>
                </h1>
                
                <?php

                /* <!-- /post title -->
    
                <!-- post details -->
                
                */
                ?>
                <span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
                <span class="author"><?php _e( 'Published by', 'ddcabarebones' ); ?> <?php the_author_posts_link(); ?></span>
                <span class="comments"><?php if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts','ddcabarebones'), __( '1 Comment','ddcabarebones'), __( '% Comments','ddcabarebones' )); ?></span>
                <?php
                /* 
                <!-- /post details --> */ 
                
                /* translators: %s: Name of current post */
                the_content( sprintf(
                    __( 'Continue reading %s', 'ddcabarebones' ),
                    the_title( '<span class="assistive-text">', '</span>', false )
                ) );

                wp_link_pages( array(
                    'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'ddcabarebones' ) . '</span>',
                    'after'       => '</div>',
                    'link_before' => '<span>',
                    'link_after'  => '</span>',
                    'pagelink'    => '<span class="assistive-text">' . __( 'Page', 'ddcabarebones' ) . ' </span>%',
                    'separator'   => '<span class="assistive-text">, </span>',
                ) );
    
                the_tags( __( 'Tags: ','ddcabarebones' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>
    
                <p><?php __( 'Categorised in: ','ddcabarebones'); the_category(', '); // Separated by commas ?></p>
    
                <p><?php __( 'This post was written by ','ddcabarebones'); the_author(); ?></p>
    
				<?php edit_post_link('Edit this','<br class="clear">');

                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif; 
                ?>
    
            </article>
            <!-- /article -->
    
        <?php endwhile; ?>
    
        <?php else: ?>
    
            <!-- article -->
            <article>
    
                <h1><?php __( 'Sorry, nothing to display.','ddcabarebones' ); ?></h1>
    
            </article>
            <!-- /article -->
    
        <?php endif; ?>
    
        </section>
        <!-- /section -->
    
        <?php get_sidebar(); ?>

	</main>

<?php get_footer(); ?>
