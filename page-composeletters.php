<?php

/**
 * Template Name: Compose Letters workflow
 *
 */

get_header(); ?>

    <main role="main">
        <!-- section -->
        <section>


            <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                <!-- article -->
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <?php the_content();

                    ?>

                    <?php edit_post_link('Edit this','<br class="clear">'); ?>
                </article>
                <!-- /article -->

            <?php endwhile;

            endif; ?>

        </section>

        <?php if (is_active_sidebar('take-action-page-widgets')) {
            ?>
            <section class="group">
                <?php
                dynamic_sidebar( 'take-action-page-widgets' );
                ?>
            </section>

        <?php } ?>

        <!-- /section -->
		<?php get_sidebar(); ?>
    </main>

<?php get_footer();
