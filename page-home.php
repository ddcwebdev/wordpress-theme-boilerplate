<?php

/**
 * Template Name: Home Page
 *
 */

get_header(); ?>
    <main role="main">
        <a id="primary-content" href="javascript:;"></a>

        <section class="carousel-container-wrapper">
            <?php
            $home_page_carousel_args = array(
                'post_type'     => 'home_page_carousel',
                'post_parent'   => 0,
                'orderby'       => 'menu_order',
                'order'         => 'asc',
            );
            $home_page_carousel = new WP_Query($home_page_carousel_args );
            ?>
            <div class="owl-carousel">

                <?php
                if ($home_page_carousel->have_posts()) : while ($home_page_carousel->have_posts()) : $home_page_carousel->the_post();

                    ?>

                    <div class="item">
                        <?php
                        echo get_the_post_thumbnail($post->ID);

                        ?>
                        <div>
                            <?php
                            the_content();
                            ?>
                        </div>
                    </div>

                    <?php

                endwhile; endif;
                wp_reset_query();

                ?>

            </div>



        <?php
            $wid = 'home-page-carousel-column-section';
            if ( is_active_sidebar( $wid ) ){
        ?>
            <div id="actions-container" class="actions-container">
                <ul>
                    <?php
                    dynamic_sidebar($wid);
                    ?>
                </ul>
            </div>
        <?php
            }
        ?>
        </section>
    </main>

<?php get_footer();    