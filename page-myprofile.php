<?php

/**
 * Template Name: My Profile
 *
 */

get_header(); ?>

    <main role="main">
		<a id="primary-content" href="javascript:;"></a>


	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
    	<section>

            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    
                <?php the_content();
    
                ?>
    
                <?php edit_post_link('Edit this','<br class="clear">'); ?>
            </article>
            <!-- /article -->
        </section>    

		<?php endwhile;

        endif; ?>

        <!-- section -->
		<?php
        if ( is_active_sidebar( 'advocate-profile-page-widgets' ) ) {
			
		?>
        <section class="group">
        <?php 
            dynamic_sidebar( 'advocate-profile-page-widgets' );
        ?>
		</section>            
		<?php            
        }
        ?>
    
		<?php 
		// Dynamically generated tabbed section for Available Alerts and Previous Alerts
		$target_widget_id = 'profile-page-tabbed-feed-section';
		if ( is_active_sidebar($target_widget_id) ) {
			$widgets = get_option('sidebars_widgets', array() );
			$side_bar_widgets = $widgets[$target_widget_id];
			?>
			<section id="my-participation" class="my-participation">
				<article class="tabs-container">
					<ul class="tabs group">
						<?php
						foreach($side_bar_widgets as $index => $widget_id) {
							$active_clause = '';
							if ($index == 0) {
								$active_clause = ' class="active"';
							}
							$widget_options = get_widget_options_from_widget_string($widget_id);
							echo "<li><a href=#" . $widget_id . $active_clause . ">" . get_widget_tab_name_from_widget_options($widget_options, $widget_id) . "</a></li>";
						}
						?>
					</ul>

				<?php dynamic_sidebar($target_widget_id); ?>
				</article>
			</section>
		<?php
		} ?>

        <!-- /section -->
		<?php get_sidebar(); ?>
    </main>

<?php get_footer();

