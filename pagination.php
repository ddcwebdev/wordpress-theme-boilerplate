<!-- pagination -->
<div class="pagination">
	<?php 
        //ddca_barebones_pagination(); 
    
          the_posts_pagination( array(
            'prev_text'          => __( 'Previous page', 'ddcabarebones' ),
            'next_text'          => __( 'Next page', 'ddcabarebones' ),
            'before_page_number' => '<span class="meta-nav assistive-text">' . __( 'Page', 'ddcabarebones' ) . ' </span>',
        ) );  
    
    ?>
</div>
<!-- /pagination -->
