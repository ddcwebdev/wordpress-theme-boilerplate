<?php get_header(); ?>

    <main role="main">
        <a id="primary-content" href="javascript:;"></a>

        <!-- section -->
        <section>


            <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                <!-- article -->
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <?php the_content();

					 echo do_shortcode('[show_nav_helper_msg]'); 
					 

					 edit_post_link('Edit this','<br class="clear">'); ?>
                </article>
                <!-- /article -->

            <?php endwhile;

            endif; ?>

        </section>
			
		<?php if (is_active_sidebar('signup-page-registration-section')) { 
				?>
            <section class="group">
		
				<?php dynamic_sidebar('signup-page-registration-section');?>
			</section>
		<?php }
		
		if ( is_active_sidebar( 'signup-page-login-section' ) ) { 
		?>
            <section class="group">
				<?php dynamic_sidebar( 'signup-page-login-section' ); ?>
			</section>
		<?php } ?>

        <!-- /section -->
        <?php get_sidebar(); ?>

    </main>

<?php get_footer();
