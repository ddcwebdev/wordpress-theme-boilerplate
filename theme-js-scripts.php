<?php
class DDCA_Barebones_Header_Scripts
{

    function __construct()
    {
        add_action('wp_enqueue_scripts', array($this, 'theme_css_and_js'), 11);
        add_action('wp_enqueue_scripts', array($this, 'sitewide_third_party_css_and_js'), 11);
        add_action('wp_enqueue_scripts', array($this, 'conditional_third_party_css_and_js'), 11);
    }

    function theme_css_and_js()
    {
        wp_enqueue_style('boilerplate-styles', get_stylesheet_uri(), array('normalize','font-awesome','jquery-sidr-bare','owl-carousel'), '');

        wp_register_script('theme-scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0',true);
        wp_enqueue_script('theme-scripts');
    }

    function sitewide_third_party_css_and_js()
    {
        function update_jquery() {
            wp_deregister_script('jquery');
            wp_register_script('jquery', "//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js", false, '3.1.1');
            wp_enqueue_script('jquery');
        }

        wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '2.1.3', 'all');
        wp_enqueue_style('normalize');

        wp_register_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.css', array(), '4.6.3', 'all');
        wp_enqueue_style('font-awesome');

        wp_register_style('jquery-sidr-bare', get_template_directory_uri() . '/css/jquery.sidr.bare.css', array(), '2.2.1', 'all');
        wp_enqueue_style('jquery-sidr-bare');

        wp_register_script('jquery-sidr', get_template_directory_uri() . '/js/plugins/jquery.sidr.js',  array('jquery'),'2.2.1' );
        wp_enqueue_script('jquery-sidr');
    }

    function conditional_third_party_css_and_js()
    {
        if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
            wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1');
            wp_enqueue_script('modernizr');
        }

        if (( strrpos(get_page_template(),'page-home.php') )) {
            wp_register_script('owl-carousel', get_template_directory_uri() . '/js/plugins/owl.carousel.js', array('jquery'), '2.0.0');
            wp_enqueue_script('owl-carousel');

            wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css', array(), '2.0.0');
        }

        if (( strrpos(get_page_template(),'page-legislatorsearch.php' ) == true ) ) {
            wp_register_script('jquery-ui-core', get_template_directory_uri() . '/js/plugins/jquery.ui.core.js', array(),'1.10.4' );
            wp_enqueue_script('jquery-ui-core');

            wp_register_script('jquery-ui-tooltip', get_template_directory_uri() . '/js/plugins/jquery.ui.tooltip.js', array(),'1.10.3' );
            wp_enqueue_script('jquery-ui-tooltip');
        }

        if (( strrpos(get_page_template(),'page-composeletters.php' ) == true ) || (strrpos(get_page_template(),'page-confirmation.php' ) == true )) {
            wp_localize_script( 'twitter_addresponse', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
        }
    }

}

$header_scripts = new DDCA_Barebones_Header_Scripts(  );