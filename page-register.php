<?php get_header(); ?>

	<main role="main">
		<a id="primary-content" href="javascript:;"></a>

		<!-- section -->
		<section>


		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content();
				
                $errorMessage =  do_shortcode('[show_nav_helper_msg]');
                if( !empty( $errorMessage ) ) {
                    echo '<p class="error">';
                    echo $errorMessage;
                    echo '</p>';
                }

                ?>


				<?php edit_post_link('Edit this','<br class="clear">'); ?>

			</article>
			<!-- /article -->

		<?php endwhile; 
		
		endif; ?>
        
		</section>

        <?php if (is_active_sidebar('registration-page-widgets')) {
            ?>
            <section class="group">
                <?php
                dynamic_sidebar('registration-page-widgets');
                ?>
            </section>

        <?php } ?>        

		<!-- /section -->
		<?php get_sidebar(); ?>
	</main>

<?php get_footer(); 
