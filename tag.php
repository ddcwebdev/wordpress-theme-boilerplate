<?php get_header(); ?>

	<main role="main">
		<a id="primary-content" href="javascript:;"></a>
		<!-- section -->
		<section>

			<h1><?php _e( 'Tag Archive: ','ddcabarebones'); echo single_tag_title('', false); ?></h1>
            <?php 
                //the_archive_title( '<h1 class="page-title">', '</h1>' ); 
                the_archive_description( '<div class="taxonomy-description">', '</div>' );            
            ?>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
		<?php get_sidebar(); ?>	
	</main>



<?php get_footer(); ?>
