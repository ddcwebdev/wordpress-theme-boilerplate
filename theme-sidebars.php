<?php
class DDCA_Barebones_Sidebars
{
    function __construct()
    {
        add_action( 'widgets_init', array($this, 'register_widgets'));
        
    }

    function register_widgets() {
        register_sidebar(
            array(
                'id'            => 'header-widgets',
                'name'          => 'Header Widget Sidebar',
                'description'   => 'Exposes widgets in the header',
                'before_widget' => '',
                'after_widget'  => '',
                'before_title'  => '',
                'after_title'   => ''
            )
        );
        
		register_sidebar(
			array(
				'id'            => 'action-alerts-page-widgets',
				'name'          => 'Action Alerts Widget Sidebar',
				'description'   => 'Exposes action alerts',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>'
			)
		);

        register_sidebar(
            array(
                'id'            => 'advocate-profile-page-widgets',
                'name'          => 'Advocate Profile Widget Sidebar',
                'description'   => 'Fills up the entire Advocate Profile page',
                'before_widget' => '',  
                'after_widget'  => '',  
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>'
            )
        );
        
        // Tabbed section in My Profile Page for Availabe/Previous Action Alerts
        register_sidebar(
            array(
                'id'            => 'profile-page-tabbed-feed-section',
                'name'          => 'Advocate Profile Tabbed Section',
                'description'   => 'Tabbed Section in My Profile Page for Available/Previous Action Alerts',
                'before_widget' => '<section id="%1$s" title="%1$s" class="tabs-content">',
                'after_widget'  => '</section>',
                'before_title'  => '',
                'after_title'   => ''
            )
        );        

		register_sidebar(
			array(
				'id'            => 'contactus-page-widgets',
				'name'          => 'Contact Us Widget Sidebar',
				'description'   => 'Fills up the entire contact us page',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>'
			)
		);
        
		register_sidebar(
			array(
				'id'            => 'committeesearch-page-widgets',
				'name'          => 'Committee Search Widget Sidebar',
				'description'   => 'Combined Committee Search and Committee Details into one page',
				'before_widget' => '<div id="%1$s" class="widget %2$s">', //by default it wraps widget in a <li>, stop that
				'after_widget'  => '</div>', //by default it wraps widget in a <li>, stop that
			)
		);
        
        register_sidebar(
            array(
                'id'            => 'editmyprofile-page-widgets',
                'name'          => 'Edit My Profile Widget Sidebar',
                'description'   => 'Fills up the entire Edit My Profile page',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',  
                'after_widget'  => '</div>',  
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>'
            )
        );        

        register_sidebar(
            array(
                'name' => __('Footer Area','ddcabarebones'),
                'id' => 'footer-widget',
                'before_widget' => '<div>',
                'after_widget' => '</div>',
                'before_title' => '<h2>',
                'after_title' => '</h2>',
            )
        );

        register_sidebar(
            array(
                'id'            => 'home-page-carousel-column-section',
                'name'          => 'Section for the Carousel Column',
                'description'   => 'Stacked Call to Actions on the right side of the carousel',
                'before_widget' => '<li>',
                'after_widget'  => '</li>',
                'before_title'  => '',
                'after_title'   => ''
            )
        );
        register_sidebar(
            array(
                'id'            => 'header-login-widgets',
                'name'          => 'Header Login Widget Sidebar',
                'description'   => 'Header Login Widget Sidebar',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>'
            )
        );

        register_sidebar(
            array(
                'id'            => 'keycontactsurvey-page-widgets',
                'name'          => 'Key Contact Survey Widget Sidebar',
                'description'   => 'Fills up the entire Key Contact Survey page',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>'
            )
        );        

		register_sidebar(
            array(
                'id'                => 'legislatorsearch-page-widgets',
                'name'              => 'Legislator Search Widget Sidebar',
                'description'       => 'Combined Legislator Search and Legislator Details into one page',
                'before_widget'     => '<div id="%1$s" class="%2$s">',
                'after_widget'      => '</div>',
                'before_title'      => '<h3 class="widget-title">',
                'after_title'       => '</h3>'
            )
        );
        
        register_sidebar(
            array(
                'id'            => 'login-page-widgets',
                'name'          => 'Login Widget Sidebar',
                'description'   => 'Center section of login page',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>'
            )
        );        

        register_sidebar(
            array(
                'id'            => 'password-assistance-page-widgets',
                'name'          => 'Password Assistance Widget Sidebar',
                'description'   => 'Adds the Password Assistance workflow to the page',
                'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                'after_widget'  => '</aside>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>'
            )
        );        
        
		register_sidebar(
			array(
				'id'            => 'registration-page-widgets',
				'name'          => 'Registration Widget Sidebar',
				'description'   => 'Exposes Registration profile selection and options',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>'
			)
		);
        
		register_sidebar(
			array(
				'id'            => 'shareyourstory-page-widgets',
				'name'          => 'Share Your Story Widget Sidebar',
				'description'   => 'Fills up the entire share your story page',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>'
			)
		);
        
        // Signup page
        register_sidebar(
            array(
                'id'            => 'signup-page-login-section',
                'name'          => 'Signup Page section for Login',
                'description'   => 'Right side/swiper of Signup Page, meant for Login Widget',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>'
            )
        );
        
        register_sidebar(
            array(
                'id'            => 'signup-page-registration-section',
                'name'          => 'Signup Page section for Registration',
                'description'   => 'Left side/swiper of Signup Page, meant for Registration Widget',
                'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                'after_widget'  => '</aside>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>'
            )
        );        
        
        
        register_sidebar(
            array(
                'id'            => 'tellafriend-page-widgets',
                'name'          => 'Tell A Friend Widget Sidebar',
                'description'   => 'Fills up the entire tell a friend page',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>'
            )
        );        

		register_sidebar(
			array(
				'id'            => 'take-action-page-widgets',
				'name'          => 'Take Action Widget Sidebar',
				'description'   => 'Fills up the entire compose letters page',
				'before_widget' => '', // by default it wraps widget in a <li>, stop that
				'after_widget'  => '', // by default it wraps widget in a <li>, stop that
			)
		);
    }



}
// Instantiate the new class
$sidebars = new DDCA_Barebones_Sidebars();