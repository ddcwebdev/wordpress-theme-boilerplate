<?php
/*
 *  Author: DDC
 *  URL: 
 *  Custom functions, support, custom post types and more.
 *  Version: .3
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/



}

/*------------------------------------*\
    js scripts Functions
\*------------------------------------*/

// Call JS scripts file
require_once(dirname(__FILE__) . '/theme-js-scripts.php');


/*------------------------------------*\
    Sidebar Functions
\*------------------------------------*/

// Call Side bar file
require_once(dirname(__FILE__) . '/theme-sidebars.php');



/*------------------------------------*\
    Place default widgets
\*------------------------------------*/

// install default widgets
require_once(dirname(__FILE__) . '/inc/default-widgets.php');

/*------------------------------------*\
    Default Shortcodes
\*------------------------------------*/

// Call side bar file
require_once(dirname(__FILE__) . '/theme-shortcodes.php');

/*------------------------------------*\
    Right Rail PHP Class
\*------------------------------------*/

include_once(dirname(__FILE__) . '/inc/ddca-barebones-rightrail.php');

/*------------------------------------*\
	Functions
\*------------------------------------*/



// Register DDCA Bare Bones Navigation
function register_ddca_barebones_menu()
{


	$locations = get_theme_mod( 'nav_menu_locations' );
	if(!empty($locations))
	{   
		foreach($locations as $location_id => $menu_value)
		{
            if ($menu_value !=0) {
                switch($location_id)
                {
                    case 'main-nav-unauthenticated':
                        $menu = get_term_by('name', 'Main Nav (Unauthenticated User)', 'nav_menu');
                    break;
        
                    case 'main-nav-authenticated':
                        $menu = get_term_by('name', 'Main Nav (Authenticated User)', 'nav_menu');
                    break;
        
                    case 'footer-nav':
                        $menu = get_term_by('name', 'Footer Nav', 'nav_menu');
                    break;
                }
        
                if(isset($menu))
                {
                    $locations[$location_id] = $menu->term_id;
                }                
            }

		}
	
		set_theme_mod('nav_menu_locations', $locations);
	}	
	
	
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}


// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
if ( ! function_exists( 'ddca_barebones_pagination' ) ) {
    /**
     * Display navigation to next/previous set of posts when applicable.
     *
     * @since Twenty Fourteen 1.0
     */
    function ddca_barebones_pagination() {
        // Don't print empty markup if there's only one page.
        if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
            return;
        }

        $paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
        $pagenum_link = html_entity_decode( get_pagenum_link() );
        $query_args   = array();
        $url_parts    = explode( '?', $pagenum_link );

        if ( isset( $url_parts[1] ) ) {
            wp_parse_str( $url_parts[1], $query_args );
        }

        $pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
        $pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

        $format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
        $format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

        // Set up paginated links.
        $links = paginate_links( array(
            'base'     => $pagenum_link,
            'format'   => $format,
            'total'    => $GLOBALS['wp_query']->max_num_pages,
            'current'  => $paged,
            'mid_size' => 1,
            'add_args' => array_map( 'urlencode', $query_args ),
            'prev_text' => __( '&larr; Previous', 'ddcabarebones' ),
            'next_text' => __( 'Next &rarr;', 'ddcabarebones' ),
        ) );

        if ( $links ) {

            ?>
            <nav class="navigation paging-navigation" role="navigation">
                <h1 class="assistive-text"><?php _e('Posts navigation', 'ddcabarebones'); ?></h1>
                <div class="pagination loop-pagination">
                    <?php echo $links; ?>
                </div><!-- .pagination -->
            </nav><!-- .navigation -->
            <?php
        }
    }
}

// Custom Excerpts
function ddca_barebones_index($length) // Create 20 Word Callback for Index page Excerpts, call using ddca_barebones_excerpt('ddca_barebones_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using ddca_barebones_excerpt('ddca_barebones_custom_post');
function ddca_barebones_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function ddca_barebones_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function ddca_barebones_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article','ddcabarebones') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Hide the Home Page Slides and Callouts CPT menu items
function remove_ccpi_cpts() {
    remove_menu_page('edit.php?post_type=callout');
    remove_menu_page('edit.php?post_type=home_page_carousel');
}

// Remove 'text/css' from our enqueued stylesheet
function ddca_barebones_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function ddca_bare_bones_gravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}
function custom_login_url($login_url) {

    return site_url('/login/');
}

// Custom Comments Callback
function ddcabarebonescomments($comment, $args, $depth)
{

    add_filter('login_url','custom_login_url');

	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>','ddcabarebones'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.','ddcabarebones') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s','ddcabarebones'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)','ddcabarebones'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif;

    remove_filter('custom_login_url');
}

function attach_template_to_page( $page_name, $template_file_name ) {

    // Look for the page by the specified title. Set the ID to -1 if it doesn't exist.
    // Otherwise, set it to the page's ID.
    $page = get_page_by_title( $page_name, OBJECT, 'page' );
    $page_id = null == $page ? -1 : $page->ID;

    // Only attach the template if the page exists
    if( $page_id != -1 ) {
        if ( ! get_post_meta( $page_id, '_wp_page_template', true )) {
            update_post_meta( $page_id, '_wp_page_template', $template_file_name );
        }
    } // end if

    return $page_id;
}
function update_page_templates(){
    $composeletters_pages = array('composeletters','additionalinformation','confirmation','mediasearch','missinginformation','notargets');
    foreach ($composeletters_pages as $page){
        attach_template_to_page($page,'page-composeletters.php');
    }
	
    $password_assistance_pages = array('resetpassword','passwordassistance');
    foreach ($password_assistance_pages as $page){
        attach_template_to_page($page,'page-passwordassistance.php');
    }	
	
}


/**
 * When getting the widgets in a sidebar, the widgets are identified by
 * class name and index. For example rss-2
 * This method extracts the index from the string. (the "2" in this example)
 * @param string $widget_string the widget string as returned by get_option('sidebar_widgets') For example: "rss-2"
 * @return integer index from the widget string (the trailing number)
 * @throws Exception when there is no number at the end of the string (prefixed by a dash '-') 
 */
function extract_widget_index_from_widget_string($widget_string)
{
  $parts = explode("-", $widget_string);
  $index_string = array_pop($parts);
  if (!isset($index_string)) {
	throw new Exception("Widget String '$widget_string' is missing an index at the end.");
  }
  return intval($index_string);
}

/**
 * When getting the widgets in a sidebar, the widgets are identified by
 * class name and index. For example rss-2
 * This method extracts the basename from the string. (the "rss" in this example)
 * @param string $widget_string the widget string as returned by get_option('sidebar_widgets') For example: "rss-2"
 * @return string input string with the index removed from the end
 */
function extract_widget_baseid_from_widget_string($widget_string)
{
  $parts = explode("-", $widget_string);
  array_pop($parts); // drop the last part
  $baseid = implode("-", $parts);
  return $baseid;
}

/**
 * When getting the widgets in a sidebar, the widgets are identified by
 * class name and index. For example rss-2
 * This method combines the baseid and index to create the key used 
 * by wordpress to store the options in the mysql database
 * @param string $base_id the base id for the widget in this example: "rss"
 * @return string option key for widget
 */
function make_options_key_for_widget($base_id)
{
  $key = implode("_", array("widget", $base_id));
  return $key;
}

/**
 * Fetch the options for the widget, identified by $widget_string
 * @return array of options stored in the mysql database for this widget
 */
function get_widget_options_from_widget_string($widget_string)
{
  $baseid = extract_widget_baseid_from_widget_string($widget_string);
  $key = make_options_key_for_widget($baseid);
  $options = get_option($key);

  $index = extract_widget_index_from_widget_string($widget_string);
  $widget_options = $options[$index];

  return $widget_options;
}

function get_widget_tab_name_from_widget_options($widget_options, $default="Untitled")
{
  $tab_name = $widget_options['title'];  
  if (empty($tab_name)) {
	$tab_name = $default;
  }
  return $tab_name;
}


// Used to block all non-CMS widget titles from rendering in all sidebars
//  Primary use case is for blocking Feed widget titles from rendering in the tabbed section of the homepage.
function block_widget_title($widget_title) {
	return '';
}


/**
 * Updates attachments urls replacing HTTP with HTTPS when necessary
 * @param  array
 * @return array URL of Attachment(S)
 */

function honor_ssl_for_srcset( $sources ) {
    if( ! is_array( $sources ) )
        return $sources;

    $serverProtocol ='';

    $serverProtocol = ( array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == 'on' ) ? 'https' : 'http';

    foreach( $sources as &$source )
    {
        if( isset( $source['url'] ) )
            $source['url'] = set_url_scheme( $source['url'], $serverProtocol );
    }
    return $sources;

};
/**
 * Updates attachments urls replacing HTTP with HTTPS when necessary
 * @param  string
 * @return string URL of Attachment
 */

function honor_ssl_for_attachments($url) {
    $http = '';
    $https = '';

    $http = site_url(FALSE, 'http');
    $https = site_url(FALSE, 'https');
    return ( array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == 'on' ) ? str_replace($http, $https, $url) : $url;
}

/*------------------------------------*\
    Custom Post Types
\*------------------------------------*/

require_once ('theme-post-types.php');

require_once(dirname(__FILE__) . '/inc/default-right-rail.php');




/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments

/**
 * This will look for the function that registers the navigation.
 * This method is most likely defined in the required Capitol Connect plugin.
 */
if (function_exists('register_custom_navigation')) {
	add_action('init', 'register_custom_navigation');
}

if (!is_admin()) {
    add_action("wp_enqueue_scripts", "update_jquery", 11);
}

add_action('init', 'register_ddca_barebones_menu'); // Add HTML5 Blank Menu
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'ddca_barebones_pagination'); // Add our HTML5 Pagination
add_action('admin_menu', 'remove_ccpi_cpts');

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

add_action('after_switch_theme', 'update_page_templates');

// Add Filters
add_filter('avatar_defaults', 'ddca_bare_bones_gravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'ddca_barebones_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'ddca_barebones_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images
add_filter('wp_get_attachment_url', 'honor_ssl_for_attachments');
add_filter('wp_calculate_image_srcset','honor_ssl_for_srcset');

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether