<?php

class DDCA_BareBones_RightRail{

    public  $startHtml;
    public  $beforeHtml;
    public  $afterHtml ;
    public  $endHtml;

    function __construct(){
        
        $this->startHtml = ( (RIGHT_RAIL_START_HTML != null)) ?  RIGHT_RAIL_START_HTML : '';
        $this->endHtml  =  ( (RIGHT_RAIL_END_HTML != null) ) ?  RIGHT_RAIL_END_HTML  : '';                
        
        $this->beforeHtml = ( (RIGHT_RAIL_BEFORE_HTML != null)) ?  RIGHT_RAIL_BEFORE_HTML : '';
        $this->afterHtml =  ( (RIGHT_RAIL_AFTER_HTML != null) ) ?  RIGHT_RAIL_AFTER_HTML  : '';        
        $this->get_right_rail();
        
    }
	
	public function get_right_rail(){
		
        //if there is a global sidebar, include it
/*         $postId = get_the_ID();
        $sidebarTop = get_post_meta($postId, 'rightrail-top-sidebar', true);
        $sidebarBottom = get_post_meta($postId, 'rightrail-bottom-sidebar', true);        
 */
		$callout_content =   $this->get_right_rail_callout_content();
		
		$have_content = $callout_content /* || is_active_sidebar( $sidebarTop ) || is_active_sidebar( $sidebarBottom ) */;
		
		if($have_content) {
		?>
		<aside id="secondary" class="sidebar" role="complementary">
        	<a id="secondary-content" href="javascript:;" tabindex="-1"></a>             
            
			<?php 
            echo $this->startHtml;
            
/* 			if (is_active_sidebar($sidebarTop)) {
				dynamic_sidebar($sidebarTop);
			} */

            
			//callouts
			echo $callout_content;
            
			//bottom
/* 			if (is_active_sidebar($sidebarBottom)) {
				dynamic_sidebar($sidebarBottom);
			} */
            
            echo $this->endHtml;
            ?>             
            
        </aside>
        <?php
		}
	}
	public function get_right_rail_callout_content(){
		// Prep output
		$sidebarContent ="";
	
		//callouts
		if (is_404()) {
            $sidebarContent .= $this->get_right_rail_for_404($sidebarContent);
			
		} else if (!is_page() && !is_search()) {
            $sidebarContent .= $this->get_right_rail_for_blog($sidebarContent);
            
		} else {

            $sidebarContent .= $this->get_right_rail_default($sidebarContent);
		}
	
		// return content
		return $sidebarContent;
	}

	public function wp_query_callouts_for_content($get_callout_items_args){
		$callout_content = '';
		$get_callout_items_loop = new WP_Query( $get_callout_items_args);
		while ($get_callout_items_loop->have_posts()):$get_callout_items_loop->the_post();		
            
            $callout_content .= $this->beforeHtml;

			$callout_content .=  do_shortcode(apply_filters( 'the_content',get_the_content())); 
            
            $callout_content .= $this->afterHtml;

		endwhile; 			
		return $callout_content;
	}

    
	public function get_right_rail_for_404($sidebarContent){
        /* placeholder for error page logic */
        $get_callout_items_args = array(
            'post_type' => 'callout',
            'category_name' => '404rightrails',
            'order' => 'asc',
            'orderby'  => 'menu_order'            
        );
        
        
        $get_callout_items_loop = new WP_Query( $get_callout_items_args);
        
        $sidebarContent .= $this->wp_query_callouts_for_content($get_callout_items_args);

        // return content
        return $sidebarContent;
    }

	public function get_right_rail_for_blog($sidebarContent){

    	$showBlogNavigation = true;
	
        /* placeholder for logic related to blog posts rendered by the single.php template */
        $get_callout_items_args = array(
            'post_type' => 'callout',
            'category_name' => 'blogrightrails',
            'order' => 'asc',
            'orderby'  => 'menu_order'
        );
        
        
        $sidebarContent .= $this->wp_query_callouts_for_content($get_callout_items_args);



        // return content
        return $sidebarContent;
    }
    
    public function get_right_rail_default($sidebarContent){
    
        // Find the current page's "callouts" meta value, it should be a comma-delimited string of post_names
       
        $callouts = get_post_meta( get_the_ID(),'rightrail-callouts',true );
        if (!empty($callouts)) {

            // Get all the post_names - these should correspond to names of custom_post_type's that are "callout"
            $calloutArray = explode(',',$callouts);

            // Search for a callout that matches the name
            //   this is inefficient but Wordpress search by name must be singular
            $get_callout_items_args = array(
                'post_type' => 'callout',
                'post_name__in' => $calloutArray
            );
            
            $sidebarContent .= $this->wp_query_callouts_for_content($get_callout_items_args);

            // return content
            return $sidebarContent;

        };
    
    }
	
}
