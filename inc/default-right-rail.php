<?php


class Default_Right_Rail_Content{
 
	
	function __construct(){

        /*
         * If the callout custom post type had never existed, then populate
         */
        add_action('after_switch_theme', array($this,'register_callouts_and_defaults'));        
        
        // Attach the Meta Box beside each page WYSIWYG
        add_action('add_meta_boxes',  array($this,'cd_meta_box_add'));        
        
        // Attached the Meta Box "save values" callback to the appropriate WP hook
        // reference: http://code.tutsplus.com/tutorials/how-to-create-custom-wordpress-writemeta-boxes--wp-20336
        add_action('save_post',  array($this,'savePageCalloutsMetaData'));
        
	}

    function register_callouts_and_defaults()
    {
        $postType = 'callout';
        
        //find if we have ever had callouts setup on this theme
        //we will use this later to determine if we should add the defaults or not.
        $calloutPosts = get_posts('post_type=' . $postType);
        $doesCalloutPostTypeExist = !empty($calloutPosts);
        
        
        // Create post object
        if (!$doesCalloutPostTypeExist) {
            // join us
            $my_post = array(
                'post_title'    => 'Join Us',
                'post_name'     => 'join-us',
                'post_content'  => '<p>Join us and become an advocate today.</p>',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     => $postType,
                // i have no idea what this is
                'post_category' => array(4, 6)
            );
            // Insert the post into the database
            wp_insert_post($my_post);
            
            // take action
            $my_post = array(
                'post_title'    => 'Take Action',
                'post_name'     => 'take-action',
                'post_content'  => '<p>Help us make a difference. Take action now!</p>',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     => $postType,
                'post_category' => array(4, 6)
            );
            // Insert the post into the database
            wp_insert_post($my_post);
            
            // tell a friend
            $my_post = array(
                'post_title'    => 'Tell a Friend',
                'post_name'     => 'tell-a-friend',
                'post_content'  => '<p>Spread the word and encourage others to get involved.</p>',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     => $postType,
                'post_category' => array(4, 6)
            );
            // Insert the post into the database
            wp_insert_post($my_post);
            
            // in the news
            $my_post = array(
                'post_title'    => 'In the News &raquo;',
                'post_name'     => 'in-the-news',
                'post_content'  => '<span>Stay up to date with the latest news, information, and current events.</span>',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     => $postType,
                'post_category' => array(4, 6)
            );
            // Insert the post into the database
            wp_insert_post($my_post);
            
            // become an advocate
            $my_post = array(
                'post_title'    => 'Become an Advocate &raquo;',
                'post_name'     => 'become-an-advocate',
                'post_content'  => '<span>Become an advocate. Registering is easy and only takes a few minutes.</span>',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     => $postType,
                'post_category' => array(4, 6)
            );
            // Insert the post into the database
            wp_insert_post($my_post);
            
            // become an advocate
            $my_post = array(
                'post_title'    => 'Share Your Story &raquo;',
                'post_name'     => 'share-your-story',
                'post_content'  => '<span>Join the discussion and share your story today.</span>',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     => $postType,
                'post_category' => array(4, 6)
            );
            // Insert the post into the database
            wp_insert_post($my_post);
        }
    }
    
    function cd_meta_box_add()
    {
        add_meta_box(
            'page-callout-management-meta-box',
            'Manage Right Rail Content',
            array( $this,'rightrailCallback'),
            'page',
            'side',
            'low'
        );
    }
    
    

    function rightrailCallback($post)
    {
        //load up the saved values to populate the options
        $callouts = get_post_meta($post->ID, 'rightrail-callouts', true);
        $sidebarTop = get_post_meta($post->ID, 'rightrail-top-sidebar', true);
        $sidebarBottom = get_post_meta($post->ID, 'rightrail-bottom-sidebar', true);
        
        // Standard Wordpress technique of verifying intent in multiple form/input POSTs
        wp_nonce_field('page_rightrails_callout_stubs_update', 'page_rightrails_callout_stubs_update_name');
        ?>
        <label for="rightrail-top-sidebar"><?php _e("Top Sidebar"); ?></label>
        <br />
        <select id="rightrail-top-sidebar" name="rightrail-top-sidebar">
            <option value="">None</option>
            <?php foreach ($GLOBALS['wp_registered_sidebars'] as $sidebar) {
                $sidebarId = ucwords($sidebar['id']);
                ?>
                <option value="<?php echo $sidebarId; ?>" <?php echo $sidebarId === $sidebarTop ? "selected" : "" ?> >
                    <?php echo $sidebar['name']; ?>
                </option>
            <?php } ?>
        </select>

        <label for="rightrail-callouts"><?php _e("Callout Stubs"); ?></label>
        <br />
        <input type="text" name="rightrail-callouts" id="rightrail-callouts" value="<?php echo $callouts; ?>" class="widefat" />

        <label for="rightrail-bottom-sidebar"><?php _e("Bottom Sidebar"); ?></label>
        <br />
        <select id="rightrail-bottom-sidebar" name="rightrail-bottom-sidebar">
            <option value="">None</option>
            <?php foreach ($GLOBALS['wp_registered_sidebars'] as $sidebar) {
                $sidebarId = ucwords($sidebar['id']);
                ?>
                <option value="<?php echo $sidebarId; ?>"  <?php echo $sidebarId === $sidebarBottom ? "selected" : "" ?>  >
                    <?php echo $sidebar['name']; ?>
                </option>
            <?php } ?>
        </select>
    <?php
    }


    function savePageCalloutsMetaData($post_id)
    {
        // Bail if we're doing an auto save
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }
        
        // if our nonce isn't there, or we can't verify it, bail
        if (!isset($_POST['page_rightrails_callout_stubs_update_name']) ||
            !wp_verify_nonce($_POST['page_rightrails_callout_stubs_update_name'], 'page_rightrails_callout_stubs_update')) {
            return;
        }
        
        // if our current user can't edit this post, bail
        if (!current_user_can('edit_post')) {
            return;
        }
        
        update_post_meta($post_id, 'rightrail-callouts', $_POST['rightrail-callouts']);
        update_post_meta($post_id, 'rightrail-top-sidebar', $_POST['rightrail-top-sidebar']);
        update_post_meta($post_id, 'rightrail-bottom-sidebar', $_POST['rightrail-bottom-sidebar']);
    }

    
    
}  

$right_rails =  new Default_Right_Rail_Content();





