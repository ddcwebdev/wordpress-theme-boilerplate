<?php
/**
 * Capitol Connect functions and definitions.
 * @package CapitolConnect
 * @subpackage Capitol_Connect
 * @since Capitol Connect 1.0
 */

class Default_Widget_Placement{
	
	function __construct(){
		/*
		 * We want to add the widgets to the pages here.
		 */
		add_action('after_switch_theme', array($this,'placeWidgetsInSideBars'), 10, 2);

	}
	/*
	 * This function gets called upon theme activation.  So, it essentially ensures that widgets
	 * are automatically put into the sidebars that define as a product, by default.
	 */
	function placeWidgetsInSideBars($oldname, $oldtheme = false)
	{
		// Save this page ID for Registration, we'll need it a bunch more times.
		$regPageId = $this->getWordpressPageIdByPageTitle('register');		
		$lSearchPageId = $this->getWordpressPageIdByPageTitle('legislatorsearch');

		// sets some default values
		$headerOptions = array(
			'title'                           => '',
			'socialmedialinks_text'           => 'Follow us on',
			'socialmedialinks_facebook'       => null,
			'socialmedialinks_facebook_url'   => '',
			'socialmedialinks_twitter'        => null,
			'socialmedialinks_twitter_url'    => '',
			'socialmedialinks_googleplus'     => null,
			'socialmedialinks_googleplus_url' => '',
			'socialmedialinks_linkedin'       => null,
			'socialmedialinks_linkedin_url'   => ''
		);
		$this->registerWidget('header-widgets', 'socialmedialinkswidget', $headerOptions);
		
		// Action Alerts page
		if (class_exists('ActionAlertsWidget')) {
			$actionAlertWidget = new ActionAlertsWidget();
			$actionAlertsOptions = $actionAlertWidget->get_default_settings_config();
			$this->registerWidget('action-alerts-page-widgets', 'actionalertswidget', $actionAlertsOptions);
		}
		
		//this one goes on the login page only
		$loginOptions = array(
			'title'                => '',
			'instructions'         => '',
			'username_label'       => 'User Name',
			'username_shadow_text' => 'Email Address',
			'password_label'       => 'Password',
			'password_shadow_text' => 'Password',
			'button_text'          => 'Log In'
		);
		$this->registerWidget('login-page-widgets', 'loginwidget', $loginOptions);
		
		//this is the login form that displays at the top of every page
		$this->registerWidget('header-login-widgets', 'loginwidget', $loginOptions);
		
		// My Profile
		//  - Only 1 of these widgets can do the unauthenticated redirect, else 3 headers will be returned
		$myProfileOptions = array(
			'title'                                  => 'My Profile',
			'unauthenticated_redirect_page_id'       => $regPageId,
			'edit_my_profile_landing_page_id'        => $this->getWordpressPageIdByPageTitle('editmyprofile'),
			'edit_my_profile_pass_through_parameter' => ''
		);
		$this->registerWidget('advocate-profile-page-widgets', 'myprofilewidget', $myProfileOptions);
		
		$myElectedOfficialsOptions = array(
			'title'                                     => 'My Elected Officials',
			'unauthenticated_redirect_page_id'          => -1,
			'federalElectedOfficialsLabel'              => 'U.S.',
			'stateElectedOfficialsLabel'                => 'States',
			'legislator_search_landing_page_id'         => $lSearchPageId,
			'legislator_search_pass_through_parameter'  => '',
			'legislator_detail_landing_page_id'         => $lSearchPageId,
			'legislator_detail_pass_through_parameter'  => '',
			'key_contact_survey_landing_page_id'        => $this->getWordpressPageIdByPageTitle('keycontactsurvey'),
			'key_contact_survey_pass_through_parameter' => ''
		);
		$this->registerWidget('advocate-profile-page-widgets', 'myelectedofficialswidget', $myElectedOfficialsOptions);
		
		$this->registerWidget(
			'advocate-profile-page-widgets',
			'text',
			'<h3 class="widget-title">My Participation</h3>'
		);
		
		// Tabbed Section in My Profile Page for Available/Previous Action Alerts
		if (class_exists('ActionAlertsWidget')) {
			$actionAlertWidget = new ActionAlertsWidget();
			$actionAlertsOptions = $actionAlertWidget->get_default_settings_config();
			$actionAlertsOptions['title'] = 'Available Actions';
			$actionAlertsOptions['suppress_title'] = 'on';
			$actionAlertsOptions['widget_template'] = 'sparse-list';
			$this->registerWidget('profile-page-tabbed-feed-section', 'actionalertswidget', $actionAlertsOptions);
		}
		if (class_exists('MyPreviousActionsWidget')) {
			$prevActionsWidget = new MyPreviousActionsWidget();
			$prevActionsOptions = $prevActionsWidget->get_default_settings_config();
			$prevActionsOptions['title'] = 'Previous Actions';
			$prevActionsOptions['suppress_title'] = 'on';
			$this->registerWidget('profile-page-tabbed-feed-section', 'MyPreviousActionsWidget', $prevActionsOptions);
		}
		
		// Take Action
		if (class_exists('TakeActionWidget')) {
			$takeActionWidget = new TakeActionWidget();
			$takeActionOptions = $takeActionWidget->get_default_settings_config();
			$this->registerWidget('take-action-page-widgets', 'takeactionwidget', $takeActionOptions);
		}
		
		// Forgot your password / Password Assistance
		$this->registerWidget(
			'password-assistance-page-widgets',
			'passwordassistancewidget',
			array(
				'username_label' => 'User Name',
			)
		);
		
		// Registration Widget for Registration Page
		$registrationOptions = array(
			'registration_profile'   =>'Full',
			'already_logged_in_text' =>'You appear to be looking for registration but you\'re already logged in.  Please log out if you wish to register again.',
			'password_label'         => 'Password',
			'confirm_password_label' => 'Confirm Password',
			'submit_button_text'     => 'Register'
		);
		$this->registerWidget('registration-page-widgets', 'registration_widget', $registrationOptions);
		
		// Signup Page needs Registration and Login widget
		$this->registerWidget('signup-page-registration-section', 'registration_widget', $registrationOptions);
		$this->registerWidget('signup-page-login-section', 'loginwidget', $loginOptions);
		// Add TAF widget to Tell A Friend Page
		$this->registerWidget(
			'tellafriend-page-widgets',
			'TellAFriendWidget',
			array(
				'form_submit_redirect_page_id'     => $this->getWordpressPageIdByPageTitle('tellafriend/thankyou'),
				'unauthenticated_redirect_page_id' => $regPageId,
				'emails_label'                     => 'Recipient Email Addresses',
				'subject_label'                    => 'Subject',
				'message_label'                    => 'Email Message',
				'body_label'                       => 'Enter your own personal message here',
				'from_name_label'                  => 'From Name',
				'from_email_label'                 => 'From Email',
				'button_text'                      => 'Tell A Friend'
			)
		);
		// Share Your Story
		$this->registerWidget(
			'shareyourstory-page-widgets',
			'ShareYourStoryWidget',
			array(
				'unauthenticated_redirect_page_id' => $regPageId,
				'form_submit_redirect_page_id'     => $this->getWordpressPageIdByPageTitle('shareyourstory/thankyou'),
				'subject_line'                     => "Share Your Story",
				'button_text'                      => "Share Your Story"
			)
		);
		// Contact Us
		$this->registerWidget(
			'contactus-page-widgets',
			'ContactUsWidget',
			array(
				'form_submit_redirect_page_id' => $this->getWordpressPageIdByPageTitle('contactus/thankyou'),
				'from_name_label'              => 'From Name',
				'from_email_label'             => 'From Email',
				'subject_label'                => 'Subject',
				'body_label'                   => 'Body',
				'button_text'                  => 'Contact Us'
			)
		);
		// Legislator Search
		$this->registerWidget(
			'legislatorsearch-page-widgets',
			'LegislatorsWidget',
			array(
				'form_submit_redirect_page_id' => $this->getWordpressPageIdByPageTitle('committeesearch')
			)
		);
		// Committee Search
		$this->registerWidget(
			'committeesearch-page-widgets',
			'CommitteesWidget',
			array(
				'form_submit_redirect_page_id' => $lSearchPageId
			)
		);
		// Edit My Profile
		$profilePageId = $this->getWordpressPageIdByPageTitle('myprofile');
		$this->registerWidget(
			'editmyprofile-page-widgets',
			'editmyprofile_widget',
			array(
				'title' => '',
				'unauthenticated_redirect_page_id'   => $regPageId,
				'form_submit_redirect_page_id'       => $profilePageId,
				'form_submit_pass_through_parameter' => '',
				'form_cancel_redirect_page_id'       => $profilePageId,
				'form_cancel_pass_through_parameter' => ''
			)
		);
		// Key Contact Survey
		$this->registerWidget(
			'keycontactsurvey-page-widgets',
			'KeyContactSurveyWidget',
			array(
				'unauthenticated_redirect_page_id' => $regPageId,
				'form_submit_redirect_page_id'     => $this->getWordpressPageIdByPageTitle('keycontactsurvey/thankyou'),
				'select_state_label'               => 'Select State',
				'select_legislative_body_label'    => 'Select Legislative Body',
				'select_legislator_label'          => 'Select Legislator',
				'how_well_known_label'             => 'How well do you know them?',
				'how_known_label'                  => 'How do you know them?',
				'details_label'                    => 'Please use the space below to fill in other details.',
				'button_text'                      => 'Submit'
			)
		);
		// Add content widgets to the homepage
		$whats_new = 'home-page-whats-new-section';
		$type = 'text';
		
		$this->appendWidget(
			$whats_new,
			$type,
			array(
				'text' => '
	<h3><a href="/">In the News &raquo;</a></h3>
	<a href="/">
	<img src="' . get_template_directory_uri() . '/img/news-back.jpg" /><span>Stay up to date with the latest news, information, and current events.</span></a>'
			)
		);
		$this->appendWidget(
			$whats_new,
			$type,
			array(
				'text' => '
	<h3><a href="/register/">Become an Advocate &raquo;</a></h3>
	<a href="/register/">
	<img src="'
		. get_template_directory_uri() . '/img/become-advocate-back.jpg" /><span>Become an advocate. Registering is easy and only takes a few minutes.</span></a>'
			)
		);
		$this->appendWidget(
			$whats_new,
			$type,
			array(
				'text' => '
	<h3><a href="/shareyourstory/">Share Your Story &raquo;</a></h3>
	<a href="/shareyourstory/">
	<img src="' . get_template_directory_uri() . '/img/share-your-story-back.jpg" /><span>Join the discussion and share your story today.</span></a>'
			)
		);
		
		// Add content widgets to the carousel column
		$carousel_column = 'home-page-carousel-column-section';
		$this->appendWidget(
			$carousel_column,
			$type,
			array(
				'text' => '<a href="register/"><h3>Join Us</h3><p>Join us and become an advocate today.</p></a>'
			)
		);
		$this->appendWidget(
			$carousel_column,
			$type,
			array(
				'text' => '<a href="actionalerts/"><h3>Take Action</h3><p>Help us make a difference. Take action now!</p></a>'
			)
		);
		$this->appendWidget(
			$carousel_column,
			$type,
			array(
				'text' => '<a href="tellafriend/"><h3>Tell a Friend</h3><p>Spread the word and encourage others to get involved.</p></a>'
			)
		);
		
		// Add general content widgets to the lower right column
		$widget_section = 'home-page-lower-right-content-section';
		$type = 'generalContentWidget';
		$contact_us_page = $this->getWordpressPageIdByPageTitle('contactus');
		
		if (!empty($contact_us_page)) {
			$page_id = $contact_us_page->ID;
			$this->appendWidget(
				$widget_section,
				$type,
				array(
					'title'                                  => 'Contact Us &raquo;',
					'general_content_landing_page_id'        => $page_id,
					'general_content_pass_through_parameter' => '',
					'general_content_featured_media_upload'  => '',
					'general_content_featured_media_embed'   => '',
					'general_content_body_text'              => '',
					'general_content_link_text'              =>
						'Have any feedback for us? We\'d love to hear it. Contact us with any questions, comments, or concerns you may have.'
				)
			);
		}
		
		$action_alert_page = $this->getWordpressPageIdByPageTitle('actionalerts');
		
		if (!empty($action_alert_page)) {
			$page_id = $action_alert_page->ID;
			$this->appendWidget(
				$widget_section,
				$type,
				array(
					'title'                                  => 'Take Action &raquo;',
					'general_content_landing_page_id'        => $page_id,
					'general_content_pass_through_parameter' => '',
					'general_content_featured_media_upload'  => '',
					'general_content_featured_media_embed'   => '',
					'general_content_body_text'              => '',
					'general_content_link_text'              => 'Your voice on the issues that matter can make a difference.  Take action!'
				)
			);
		}
		
		// Add Tabbed Feeds to lower left section in homepage
		$this->registerWidget(
			'home-page-tabbed-feed-section',
			'TwitterFeedWidget',
			array(
				'text'   => '<a class="twitter-timeline" href="https://twitter.com/DDCAdvocacy" data-widget-id="596805481684938752">Tweets by @DDCAdvocacy</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>',
				'filter' => false,
				'title'  => 'Twitter'
			)
		);
		$this->registerWidget(
			'home-page-tabbed-feed-section',
			'FacebookFeedWidget',
			array(
				'text'  => '<div id="fb-root"></div><script>(function(d, s, id) { var js, fjs = 
d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; 
js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=2345574733"; 
fjs.parentNode.insertBefore(js, fjs); }(document, "script", "facebook-jssdk"));</script>
	<div class="fb-page" data-href="https://www.facebook.com/DDCPublicAffairs" data-width="500" data-height="450" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/DDCPublicAffairs"><a href="https://www.facebook.com/DDCPublicAffairs">DDC</a></blockquote></div></div>',
				'title' => 'Facebook'
			)
		);

        
        $this->registerWidget(
            'footer-widget',
            'text',
            array(
                'text'  =>  '&copy; [year]; Copyright ' .  get_bloginfo ('name') . ''
            )            
        );
        
        $menu = get_term_by('name', 'Footer Nav', 'nav_menu');

        $this->registerWidget(
            'footer-widget',
            'nav_menu',
            array(
                'title' => '',
                'nav_menu' => $menu->term_id
                
            )            
        );
        
        
	}
	
	/**
	 * @desc Registers widget to given sidebar.  If widget already exists, it will NOT add it again.
	 * 
	 * @param string $sidebarId : ID of the sidebar to place widget in.  Defined in the theme.
	 * @param string $widgetName : Widget name in lowercase.
	 * @param array $widgeOptions : Array of options for defaults of widget properties.
	 * @param boolean $appendMode : Flag, if set - will override the uniqueness constraint and append another instance
	 */
	function registerWidget($sidebarId, $widgetName, $widgetOptions = array(), $appendMode = false)
	{
		//  Be sure to switch the widgetName to lower case since we are by-passing the WP options methods
		$widgetName = strtolower($widgetName);
		$sidebars_widgets = get_option('sidebars_widgets');
		// see if widget already exists
		$exists = false;
		// Track the known instances' Ids to adhere to WordPress's unique Id tracking
		// This will be an array of integers so we can sort later
		$known_instances = array();
		
		// loop through all sidebars
		foreach ($sidebars_widgets as $savedSidebarId => $sidebar_widgets) {
			// loop through all widgets within the sidebar
			if (is_array($sidebar_widgets)) {
				foreach ($sidebar_widgets as $widget) {
					// check to see if this type of widget is already in this specific side bar
					// WP will append a number after the widget (e.g. login-widget becomes login-widget-10)
					// drop the bits after the name and do the check to find it
					if (substr($widget, 0, strlen($widgetName)) === $widgetName) {
						// If this is the targeted sidebar, remember that the targetted type exists
						if ($savedSidebarId === $sidebarId) {
							$exists = true;
						}
						// Add to our list of known Ids of all widgets of this type so we don't create a collision
						// Grab the last characters from a split on the dash/hyphen (e.g. login-widget-10, we want 10)
						//  and ensure it's an integer when being pushed into the known_instances array
						$parsed_id = intval(array_pop(explode('-', $widget)));
						if (!empty($parsed_id)) {
							array_push($known_instances, $parsed_id);
						}
					}
				}
			}
		}
		
		// if the widget is not in the sidebar already, or we want another instance of that type - add it
		if (!$exists || $appendMode) {
			// increment from the largest known Id of this type to adhere to WP behavior
			if (empty($known_instances)) {
				$highest_known_id = 0;
			} else {
				// sort lowest to highest
				sort($known_instances);
				$highest_known_id = array_pop($known_instances);
			}
			
			$id = $highest_known_id + 1;
			// build the widget uid string
			$uid = $widgetName . '-' . $id;
			
			// if we have the sidebar, push widget to existing list
			if (!empty($sidebars_widgets[$sidebarId])) {
				array_push($sidebars_widgets[$sidebarId], $uid);
			} else {
				// create a new list of widgets with a count of 1 (our new widget)
				$sidebars_widgets[$sidebarId] = array($uid);
			}
			
			// gets the options currently with the widget
			$ops = get_option('widget_' . $widgetName);
			$insert = true;
			
			// If we're in append mode, we still want to ensure we're not duplicating an existing configuration
			if ($appendMode && is_array($ops)) {
				$widget_ids = array_keys($ops);
				foreach ($widget_ids as $existing_id) {
					// array_diff_assoc will return an empty array if it matches $widgetOptions
					//   A match is defined as both one-way diffs matching  (linear algebra ftw)
					if (is_array($ops[$existing_id]) && 
						(array_diff_assoc($ops[$existing_id], $widgetOptions) === array_diff_assoc($widgetOptions, $ops[$existing_id]))) {
						// stop everything, flag not to insert into the database
						$insert = false;
						break;
					}
				}
			}
			
			// Insert into database if we're clear to do so
			if (isset($insert) && ($insert === true)) {
				// sets some default values
				$ops[$id] = $widgetOptions;
				
				// puts back into the DB (assigned to correct sidebar)
				update_option('widget_' . $widgetName, $ops);
				update_option('sidebars_widgets', $sidebars_widgets);
			}
		}
	}
	
	/**
	 * @desc Appends widget to given sidebar.
	 * 
	 * @param string $sidebarId : ID of the sidebar to place widget in.  Defined in the theme.
	 * @param string $widgetName : Widget name in lowercase.
	 * @param array $widgeOptions : Array of options for defaults of widget properties.
	 */
	function appendWidget($sidebarId, $widgetName, $widgetOptions)
	{
		$this->registerWidget($sidebarId, $widgetName, $widgetOptions, true);
	}
	
	function getWordpressPageIdByPageTitle($pageTitle)
	{
		$rt = -1;
		$page = get_page_by_path($pageTitle);
       
        
		if ($page->ID >= 0) {
			$rt = $page->ID;
		}
		return $rt;
	}



}

$default_widget_placement = new Default_Widget_Placement();