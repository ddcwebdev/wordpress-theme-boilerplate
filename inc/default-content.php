<?php
/**
 * Capitol Connect functions and definitions.
 * @package CapitolConnect
 * @subpackage Capitol_Connect
 * @since Capitol Connect 1.0
 */

class Default_Content_Placement{

	function __construct(){
	
		/*
		 * We want to add the widgets to the pages here.
		 */
		add_action('after_switch_theme', array($this,'set_default_content'), 10);
	}
	/*
	 * This function gets called upon theme activation.  So, it essentially ensures that widgets
	 * are automatically put into the sidebars that define as a product, by default.
	 */
	function set_default_content()
	{
		
		updateEmptyPageContentByTitle(
			'CarouselSlide-1', 
			'<h2>Join Us Today</h2><p>Help us strengthen our presence and make our voice heard.</p><p><a href="#">Become an advocate today!</a></p>
',
			'',
			'home_page_carousel'
		);
	}
	
	// Helper function - updates the page content iff (if-and-only-if) it's empty
	function updateEmptyPageContentByTitle($pageTitle, $newContent,$post_type = 'page')
	{
		$rt = -1;
		$wp_error = '';
		// Get the unique page ID and page content for our target
		$page = get_page_by_title($pageTitle);
		
		if (!empty($page)) {
			$id = $page->ID;
			$currentContent = $page->post_content;
			
			// If the currentContent is empty, update it with the new content
			if (empty($currentContent)) {
				$wp_error = wp_update_post(
					array(
						'ID'           => $id,
						'post_content' => $newContent,
						'post_type' => $post_type
					),
					// this returns the WP_Error object if the DB update goes wrong
					true
				);
			}
		}
		
		// Check if we had issues
		if (is_wp_error($wp_error)) {
			foreach ($wp_error->get_error_messages() as $error) {
				error_log($error);
			}
		} else {
			// Success!
			$rt = 0;
		}
		
		return $rt;
	}	
}