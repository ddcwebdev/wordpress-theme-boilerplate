<?php

// NOTICE: Do NOT use "callout" or "home_page_carousel" as the name for any of your post types. These are in use by the CapConnect Plugin!

class Create_Custom_Post_Types {
    public function add_content_type($content_type_name,$singular_name,$plural_name,$cpt_supports,$cpt_hierarchical,$show_in_search){
        $labels = array(
            'name'               => ucwords($singular_name),
            'singular_name'      => ucwords($singular_name),
            'menu_name'          => ucwords($plural_name),
            'name_admin_bar'     => ucwords($singular_name),
            'add_new'            => 'New ' . ucwords($singular_name),
            'add_new_item'       => 'Add New ' . ucwords($singular_name),
            'new_item'           => 'New ' . ucwords($singular_name),
            'edit_item'          => 'Edit ' . ucwords($singular_name),
            'view_item'          => 'View ' . ucwords($plural_name),
            'search_items'       => 'Search ' . ucwords($plural_name),
            'parent_item_colon'  => 'Parent ' . ucwords($plural_name) . ':',
            'not_found'          => 'No ' . ucwords($plural_name) . ' found.',
            'not_found_in_trash' => 'No ' . ucwords($plural_name) . ' found in Trash.',
        );

        $args = array(
            'labels'            => $labels,
            'public'            => true,
            'exclude_from_search' => $show_in_search,
            'publicly_queryable'=> true,
            'show_ui'           => true,
            'show_in_nav'       => true,
            'show_in_nav_menus' => true,
            'query_var'         => true,
            'hierarchical'      => $cpt_hierarchical,
            'supports'          => $cpt_supports,
            'has_archive'       => true,
            'menu_position'     => null,
            'show_in_admin_bar' => true,
            'capability_type'   => 'post',
            'rewrite'           => array(
                'with_front' => false
            )
        );

        //register your content type
        register_post_type($content_type_name, $args);

    }

    public function add_custom_taxonomy($cpt_name,$taxonomy_name,$singular_taxonomy_name,$plural_taxonomy_name,$taxonomy_hierarchical){

        $labels = array(
            'name'                       => ucwords($plural_taxonomy_name),
            'singular_name'              => ucwords($singular_taxonomy_name),
            'search_items'               => __( 'Search ','ddcabarebones') . ucwords($plural_taxonomy_name) ,
            'all_items'                  => __( 'All ','ddcabarebones') . ucwords($plural_taxonomy_name) ,
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'edit_item'                  => __( 'Edit ','ddcabarebones') . ucwords($singular_taxonomy_name) ,
            'update_item'                => __( 'Update ','ddcabarebones') . ucwords($singular_taxonomy_name) ,
            'add_new_item'               => __( 'Add ','ddcabarebones') . ucwords($singular_taxonomy_name),
            'new_item_name'              => __( 'New ','ddcabarebones') . ucwords($singular_taxonomy_name),
            'add_or_remove_items'        => __( 'Add or remove ','ddcabarebones' ) .  $plural_taxonomy_name,
            'not_found'                  => __( 'No ','ddcabarebones'). ucwords($plural_taxonomy_name) . __(' found. '),
            'menu_name'                  => ucwords($plural_taxonomy_name),
        );

        $story_category_args = array(
            'hierarchical'          => $taxonomy_hierarchical,
            'labels'                => $labels,
            'show_ui'               => true,
            'show_admin_column'     => true,
            'query_var'             => true,
            'rewrite'               => array(
                'hierarchical' => $taxonomy_hierarchical,
                'with_front' => false
            )
        );

        register_taxonomy( $taxonomy_name , $cpt_name, $story_category_args );

    }






}