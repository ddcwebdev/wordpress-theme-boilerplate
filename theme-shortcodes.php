<?php


class DDCA_BareBones_Shortcodes{
    function __construct(){
        add_shortcode('year', array($this, 'year_shortcode'));
        add_shortcode('file_path', array($this, 'file_path'));
        add_shortcode('email', array($this, 'hide_email_shortcode' ));
        add_shortcode('redirect', array($this, 'redirect'));
        
    }
    /* Short Code to Print year out dynamically  */
    /* [year] */
    function year_shortcode() {
        $year = date('Y');
        return $year;
    }


    /*
    [file_path]
    */
    function file_path() {
        ob_start();

        $theme_uri = is_child_theme()
            ? get_stylesheet_directory_uri()
            : get_template_directory_uri();

        return trailingslashit( $theme_uri );
    }



    /*
    [email]ddc@ddc.com[/email]
    converted to this <a href="mailto:dd&#99;&#64;d&#100;&#99;&#46;c&#111;&#109;">&#100;&#100;c&#64;&#100;d&#99;.&#99;om</a>
    */
    function hide_email_shortcode( $atts , $content = null ) {
        if ( ! is_email( $content ) ) {
            return;
        }

        return '<a href="mailto:' . antispambot( $content ) . '">' . antispambot( $content ) . '</a>';
    }

    /* [redirect]URL[/redirect] */

    function redirect($atts, $content=null){
        if (isset($content)){
            header('Location:' . $content);
            exit;
        }

    }


}


$shortcodes = new DDCA_BareBones_Shortcodes();