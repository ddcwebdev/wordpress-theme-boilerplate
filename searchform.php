<!-- search -->
<form class="search" method="get" action="<?php echo home_url(); ?>" role="search">
	<input class="search-input" type="search" name="s" placeholder="<?php __( 'To search, type and hit enter.','ddcabarebones'); ?>">
	<button class="search-submit" type="submit" role="button"><?php __( 'Search','ddcabarebones'); ?></button>
</form>
<!-- /search -->
