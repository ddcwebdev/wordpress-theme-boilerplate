<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head();

		$advocate_logged_in ='';
		if (function_exists('\Ddc\supported\isAdvocateLoggedIn')){
			$advocate_logged_in = \Ddc\supported\isAdvocateLoggedIn();
		}
        ?>
	</head>
	<body <?php body_class(); ?> >
		<?php 
		

		
		?>
		<!-- wrapper -->
		<div class="wrapper">

			<!-- header -->
			<header class="header group" role="banner">
				<div class="hot-bar-container group">
					<?php if (is_active_sidebar('header-widgets')) {
						echo '<div>';
						dynamic_sidebar('header-widgets');
						echo '</div>';
					} ?>
					<?php if ($advocate_logged_in) { ?>
						<p>Welcome, <a href="<?php echo site_url('/myprofile/'); ?>"><?php echo getAdvocate_FirstName(); ?></a> |
							<a href="<?php echo site_url('/Logout/'); ?>">Log out</a></p>
					<?php } elseif (!is_page('resetpassword')) { ?>
						<p>If you're already a member, please <a href="#">log in</a>.
							If not, please <a href="<?php echo site_url('/register/'); ?>">register</a>.</p>
					<?php } ?>

				</div>
				<!-- logo -->
				<div class="logo">
					<a href="<?php echo home_url(); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="Logo" class="logo-img">
					</a>
				</div>
				<!-- /logo -->

				<!-- nav -->
				<nav class="nav" role="navigation">
					<h2 class="assistive-text"><?php _e( 'Main menu','ddcabarebones'); ?></h2>
					<?php /* Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff. */ ?>
					<div class="skip-link"><a class="assistive-text" href="#primary-content" title="<?php esc_attr_e( 'Skip to primary content', 'ddcabarebones' ); ?>"><?php _e( 'Skip to primary content', 'ddcabarebones' ); ?></a></div>

					<?php if (!strrpos(get_page_template(),'page-home.php')){ ?>
						<div class="skip-link"><a class="assistive-text" href="#secondary-content" title="<?php esc_attr_e( 'Skip to secondary content', 'ddcabarebones' ); ?>"><?php _e( 'Skip to secondary content', 'ddcabarebones' ); ?></a></div>

					<?php } ?>

                    <div class="sidr-button"><a class="sidr-button-close" href="javascript:;"></a></div>

                    <a href="javascript:;" class="mobile-nav-btn"><i class="fa fa-bars fa-lg" title="Menu"></i></a>

                    <?php
					$main_nav_name = MAIN_NAV_AUTHENTICATED_USER;
					if (!$advocate_logged_in) {
						$main_nav_name = MAIN_NAV_UNAUTHENTICATED_USER;
						$signUpUrl = site_url('/signup/');
					}
					wp_nav_menu(
						array(
							'menu'            => $main_nav_name,
							'container_class' => 'menu-main-nav-menu-container'
						)
					);

					?>
				</nav>
				<!-- /nav -->
				<?php
				if (!$advocate_logged_in && (!is_page('resetpassword') && !is_404() && is_active_sidebar('header-login-widgets'))) {
					?>
					<div class="login-form header-login-form">
						<?php dynamic_sidebar('header-login-widgets'); ?>
					</div>
					<?php
				}
				?>
			</header>
			<!-- /header -->
