<?php get_header(); ?>

	<main role="main">
		<a id="primary-content" href="javascript:;"></a>

		<!-- section -->
		<section>

			<h1><?php echo sprintf( __( '%s Search Results for ','ddcabarebones' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
		<?php get_sidebar(); ?>

	</main>
<?php get_footer(); ?>
